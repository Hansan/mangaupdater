CXX = clang++
LIBS = -lcurl -ltidy
BINDIR = bin
BIN = mud
TESTBIN = test
OBJ = mud
SRC = mangaupdater.cpp libmud.cpp json.c
LIBDIR = .
LIBSRC = libmud.cpp
TMPDIR = bin
CFLAGS = -Wall -Wextra -Wpedantic -Wshadow -std=c++17

ifeq ($(OS),Windows_NT)
	BIN := $(BIN).exe
	TESTBIN := $(BIN).exe
endif

debug:
	${CXX} ${CFLAGS} -g -o ${BINDIR}/${BIN} ${SRC} ${LIBS}

qt:
	${CXX} ${CFLAGS} -g -o ${BINDIR}/qt-mud.exe mangaupdater-qt.cpp libmud.cpp json.c ${LIBS} -lQt5Core.dll -lQt5Gui.dll -lQt5Widgets.dll

run: qt
	${BINDIR}/qt-mud.exe

lib:
	${CXX} -g -c ${CFLAGS} -o ${TMPDIR}/${OBJ}.o ${LIBSRC}
	ar rcs ${LIBDIR}/lib${OBJ}.a ${TMPDIR}/${OBJ}.o
	rm ${TMPDIR}/${OBJ}.o

release:
	${CXX} ${CFLAGS} -O2 -o ${BINDIR}/${BIN} ${SRC} ${LIBS}

test:
	${CXX} ${CFLAGS} -g -o ${BINDIR}/${TESTBIN} test.cpp json.c libmud.cpp ${LIBS}

clean:
	rm ${BINDIR}/${BIN}
