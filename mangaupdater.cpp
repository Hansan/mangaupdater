#include <cassert>
#include <iomanip>
#include <iostream>
#include <string>

#include "libmud.hpp"

enum class Action {
    NONE,
    DOWNLOAD,
    UPLOAD,
    UPDATE_MANGA,
    SHOW_LIST,
    SHOW_UNREAD,
};

auto usage()
{
    std::cerr << "-d download\n";
    std::cerr << "-u upload\n";
    std::cerr << "-m update manga\n";
    std::cerr << "-s show unread manga\n";
    std::cerr << "-sall show list\n";
}

auto get_arg_option(int argc, char** argv) -> Action
{
    auto arg = std::string_view { argv[1] };
    auto action = Action::NONE;
    if (arg == "-d") {
        action = Action::DOWNLOAD;
    } else if (arg == "-u") {
        action = Action::UPLOAD;
    } else if (arg == "-m") {
        if (argc < 4) {
            std::cerr << "not enough arguments\n";
            return action;
        }
        action = Action::UPDATE_MANGA;
    } else if (arg == "-s") {
        action = Action::SHOW_UNREAD;
    } else if (arg == "-sall") {
        action = Action::SHOW_LIST;
    }

    return action;
}

auto main(int argc, char** argv) -> int
{
    if (argc < 2) {
        usage();
        return 0;
    }

    auto action = get_arg_option(argc, argv);

    switch (action) {
    case Action::NONE: {
        return 0;
    } break;
    case Action::DOWNLOAD: {
        auto list = mud::download_list();
        mud::save_list(list);
    } break;
    case Action::UPLOAD: {
        if (auto list = mud::load_list(); list) {
            mud::upload_list(*list);
        }
    } break;
    case Action::UPDATE_MANGA: {
        if (auto list = mud::load_list(); list) {
            auto mangaName = argv[2];
            auto manga = mud::get_manga_from_list(mangaName, *list);
            if (!manga) {
                std::cerr << "Error could not find manga in list\n";
            } else {
                auto newval = atoi(argv[3]);
                std::cout << manga->name << '\n';
                std::cout << manga->readCh << " -> " << newval << '\n';
                manga->readCh = newval;
                mud::save_list(*list);
            }
        }
    } break;
    case Action::SHOW_LIST: {
        if (auto list = mud::load_list(); list) {
            for (auto& manga : *list) {
                std::cout << std::setw(3) << manga.readCh << '/' << std::setw(3) << manga.totalCh << " | " << manga.name << '\n';
            }
        }
    } break;
    case Action::SHOW_UNREAD: {
        if (auto list = mud::load_list(); list) {
            for (auto& manga : *list) {
                if (manga.readCh < manga.totalCh) {
                    std::cout << std::setw(3) << manga.readCh << '/' << std::setw(3) << manga.totalCh << " | " << manga.name << '\n';
                }
            }
        }
    } break;
    default: {
        assert(false && "Arg option not implemented");
    } break;
    } // switch
}
