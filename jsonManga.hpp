#pragma once

#include "libmud.hpp"

#include <filesystem>
#include <optional>

auto json_load_list(std::filesystem::path file) -> std::optional<MangaList>;
auto json_save_list(MangaList const& list, std::filesystem::path file) -> bool;
