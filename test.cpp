#include <iostream>

#include "libmud.hpp"

#define softassert(condition) (std::cerr << ((condition) ? "SUCCESS" : "FAILURE") << ':' << __LINE__ << ':' << __func__ << " - (" #condition ")\n")

namespace test {

auto load_from_json() -> void {
    auto list = mud::load_list();
}

auto save_to_json() -> void;
auto download_list() -> void;
auto update_list() -> void;
auto upload_list() -> void;
auto get_manga() -> void;
auto mangaupdates_get_manga() -> void;
auto add_manga() -> void;

} // namespace test

int main()
{
    softassert(true);
    softassert(43);
    softassert(0);
    softassert(false);
    test::load_from_json();
    // test::save_to_json();
    // test::download_list();
    // test::update_list();
    // test::upload_list();
    // test::get_manga();
    // test::mangaupdates_get_manga();
    // test::add_manga();
}
