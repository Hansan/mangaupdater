#include "libmud.hpp"

#include "htmlDocument.hpp"
#include "jsonManga.hpp"
#include "managedTidy.hpp"

#include "jint.h"

#include <curl/curl.h>

#include <tidy.h>
#include <tidybuffio.h>

#include <fmt/compile.h>
#include <fmt/core.h>

#include <cassert>
#include <charconv>
#include <cstdio>
#include <iomanip>
#include <iostream>
#include <memory>
#include <optional>
#include <string>
#include <vector>

#define PROGRAM_NAME "mud"

#define DEBUG 1

using namespace std::literals;

enum class HtmlListPage {
    USERLIST,
    SEARCH,
};

struct Link {
    std::string url;
    std::string name;
};

using LinkList = std::vector<Link>;

constexpr auto cookieFilename = "data/cookies.txt";
constexpr auto credentialsFilename = "data/credentials.txt";
constexpr auto mangaListFilename = "data/mangaList.json";

CURL* curl;

namespace mud {

auto visit_site() -> void;
auto get_id_from_url(std::string_view url) -> size_t;
auto mangaupdates_get_manga_from_page(Link link) -> Manga;
auto mangaupdates_find_manga_url(std::string_view name) -> Link;
auto pull_list(MangaList* mangaList) -> bool;
auto is_sub_string(char const* substr, char const* str) -> bool;
auto get_manga_id(char const* arg, MangaList const* list) -> size_t;
auto update_manga(MangaList* mangaList, int index, int readCh) -> void;

auto tidy_buffer_write_callback(void* in, size_t const size, size_t const nmemb, void* out) -> size_t;
auto noop_tidy_buffer_write_callback(void const* in, size_t size, size_t nmemb, void const* out) -> size_t;

auto find_list_node(TidyDoc tree, HtmlListPage const page) -> TidyNode;
auto get_manga_title_from_node(TidyDoc tdoc, TidyNode node) -> std::optional<std::string>;
auto find_manga_title_node(TidyNode const rootNode) -> TidyNode;
auto get_latest_chapter_from_node(TidyDoc tdoc, TidyNode node, Manga* manga) -> bool;
auto find_latest_chapter_node(TidyNode rootNode) -> TidyNode;
auto get_manga_id_from_node(TidyNode const mangaNode) -> std::optional<size_t>;
auto get_read_chapters_from_node(TidyDoc const tdoc, TidyNode const node) -> std::optional<size_t>;
auto find_read_chapters_node(TidyNode rootNode) -> TidyNode;
auto get_manga_from_node(TidyDoc const tdoc, TidyNode const mangaNode) -> std::optional<Manga>;

auto create_credentials_file(std::string_view filename) -> void;
auto read_credentials(FILE* file) -> void;
auto login(void) -> void;
auto find_regex_manga(std::string_view searchName, MangaList const& list) -> std::vector<size_t>;
auto reset_curl(void) -> void;

HtmlDocument::HtmlDocument(std::string url)
    : m_document { tidyCreate() }
    , m_url { std::move(url) }
{
    if (!curl) {
        login();
    }

    auto errBuf = ManagedTidyBuffer {};
    setErrorBuffer(errBuf);

    auto docBuf = ManagedTidyBuffer {};

    // Write html string to a TidyBuffer
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &docBuf.get());
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, tidy_buffer_write_callback);
    curl_easy_setopt(curl, CURLOPT_URL, m_url.c_str());

    visit_site();
    reset_curl();

    // print cookies to file or stdout (-) at cleanup
    /* curl_easy_setopt(curl, CURLOPT_COOKIEJAR, cookieFilename); */

    // Parse and clean html tree
    auto err = tidyParseBuffer(m_document, &docBuf.get());
    if (err >= 0) {
        /* std::cout << "cleaning\n"; */
        err = tidyCleanAndRepair(m_document);
        if (err >= 0) {
            /* std::cout << "diagnosing\n"; */
            err = tidyRunDiagnostics(m_document);
            if (err >= 0) {
                /* std::cerr << errBuf.bp << '\n'; */
            }
        }
    }
}

// fill out a manga struct array with data retrieved from a TidyDoc of the mangaupdates list
auto download_list() -> MangaList
{
    MangaList list;
    auto document = HtmlDocument { "https://www.mangaupdates.com/mylist.html" };

    auto listNode = find_list_node(document.document(), HtmlListPage::USERLIST);
    assert(listNode);

    for (auto mangaNode = tidyGetChild(listNode);
         mangaNode;
         mangaNode = tidyGetNext(mangaNode)) {
        if (auto currManga = get_manga_from_node(document.document(), mangaNode); currManga) {
            // add populated manga to list
            list.push_back(std::move(*currManga));
        }
    }

    return list;
}

auto load_list() -> std::optional<MangaList>
{
    return json_load_list(mangaListFilename);
}

static auto upload_manga(Manga const& manga, int mangaUpdateCount) -> void
{
    constexpr auto updateUrl = FMT_STRING("https://www.mangaupdates.com/ajax/chap_update.php?s={}&inc_c={}");
    char finalUrl[256] {};

    // first update by as many tens as possible
    auto tenCount = mangaUpdateCount / 10;
    if (tenCount < 0) {
        tenCount = -tenCount;
    }

    auto percent = 0.0;
    std::cout << "  0%" << std::flush;

    for (auto k = 0; k < tenCount; ++k) {
        auto const updateAmount = mangaUpdateCount < 0 ? -10 : 10;

        fmt::format_to_n(finalUrl, 255, updateUrl, manga.id, updateAmount);

        // REQUEST
        curl_easy_setopt(curl, CURLOPT_URL, finalUrl);
        visit_site();

        // display percentage
        percent += (100.0 * updateAmount) / mangaUpdateCount;
        std::cout << "\r  " << std::setprecision(1) << percent << '%' << std::flush;
    }

    // then update by the remainder
    if (mangaUpdateCount % 10) {
        fmt::format_to_n(finalUrl, 255, updateUrl, manga.id, mangaUpdateCount % 10);

        // REQUEST
        curl_easy_setopt(curl, CURLOPT_URL, finalUrl);
        visit_site();

        // display percentage
        percent += 100.0 * (double)(mangaUpdateCount % 10) / (double)mangaUpdateCount;
        std::cout << "\r  " << std::setprecision(1) << percent << '%' << std::endl;
    }
}

auto upload_list(MangaList& mangaList) -> void
{
    auto extList = download_list();

    // disable printing of request response
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, noop_tidy_buffer_write_callback);

    // TODO: make binary search?
    for (auto& localManga : mangaList) {
        auto extMangaIt = std::find_if(extList.begin(), extList.end(), [&localManga](const auto& extManga) {
            return localManga.id == extManga.id && localManga.readCh != extManga.readCh;
        });
        if (extMangaIt != extList.end()) {
            auto& extManga = *extMangaIt;
            s32 const mangaUpdateCount = localManga.readCh - extManga.readCh;
            // since the find_if makes sure the local and external readCh members
            // are different, this shouldn't ever happen.
            assert(mangaUpdateCount != 0);

            // display data for update
            std::cout << extManga.readCh << " -> " << localManga.readCh << " | " << localManga.name << '\n';

            upload_manga(localManga, mangaUpdateCount);
        }
    }

    // print cookies to file or stdout (-) at cleanup
    curl_easy_setopt(curl, CURLOPT_COOKIEJAR, cookieFilename);
    curl_easy_cleanup(curl);
}

auto cleanup() -> void
{
    if (curl) {
        curl_easy_setopt(curl, CURLOPT_COOKIEJAR, cookieFilename);
        curl_easy_cleanup(curl);
    }
}

auto pull_list(MangaList* mangaList) -> bool
{
    assert(mangaList);

    auto extList = download_list();

    auto changes = false;

    // check if local list is newer than ext list
    // should also check for new manga added to extList
    for (auto extManga : extList) {
        auto localMangaIt = std::find_if(mangaList->begin(), mangaList->end(), [&extManga](const auto& localManga) {
            return localManga.id == extManga.id;
        });
        if (localMangaIt == mangaList->end()) {
            // if the extList.id wasn't found, add the manga to the mangaList;

            changes = true;

            std::cout << "Adding new manga:\n";
            std::cout << extManga.readCh << '/' << extManga.totalCh << " | " << extManga.name << '\n';

            mangaList->push_back(extManga);
            continue;
        }
        const auto& localManga = *localMangaIt;
        changes = extManga.readCh != localManga.readCh || extManga.totalCh != localManga.totalCh;
        if (changes && extManga.readCh < localManga.readCh) {
            std::cout << "Local list is newer than external list. Pull anyway? y/n\n";

            auto choice = '\0';

            do {
                std::cout << "> " << std::flush;

                scanf("%c", &choice);

                choice = tolower(choice);

                int c;
                while ((c = getchar()) != '\n' && c != EOF) {
                }; // flush stdin
            } while (choice != 'y' && choice != 'n');

            if (choice == 'n') {
                return false;
            }
            if (choice == 'y') {
                break;
            }
        }
    }

    if (changes) {
        // print out changes
        for (auto extManga : extList) {
            for (auto localManga : *mangaList) {
                if (extManga.id != localManga.id) {
                    continue;
                }

                if (extManga.totalCh != localManga.totalCh) {
                    std::cout << "chapters: " << localManga.totalCh << " -> " << extManga.totalCh << " | " << extManga.name << '\n';
                }
                if (extManga.readCh != localManga.readCh) {
                    std::cout << "read chapters: " << localManga.readCh << " -> " << extManga.readCh << " | " << extManga.name << '\n';
                }
            }
        }

        if (!save_list(extList)) {
            std::cout << "error saving database\n";
            return false;
        }
    } else {
        std::cout << "Local list is up to date\n";
    }
    return true;
}

auto get_manga_from_list(std::string_view name, MangaList& list) -> Manga*
{
    auto mangaStack = find_regex_manga(name, list);

    Manga* chosenManga = nullptr;
    if (mangaStack.size() > 1) {
        std::cout << "Choose (" << name << "):\n";
        std::cout << "[ 0] EXIT\n";

        // print out the manga in the stack
        for (auto i = 0ull; i < mangaStack.size(); ++i) {
            auto const index = mangaStack[i];
            auto const& manga = list[index];

            std::cout << "[" << std::setw(2) << i + 1 << "] " << std::setw(3) << manga.readCh << '/' << std::setw(3) << manga.totalCh << " | " << manga.name << '\n';
        }

        auto choice = -1;

        do {
            std::cout << "> " << std::flush;

            scanf("%d", &choice);
            int c;
            while ((c = getchar()) != '\n' && c != EOF) {
            }; // flush stdin
        } while (choice < 0 || size_t(choice) > mangaStack.size());

        if (!choice) {
            return 0;
        }

        auto const index = mangaStack[choice - 1];
        chosenManga = &list[index];
    } else if (mangaStack.size() == 1) {
        auto const index = mangaStack[0];
        chosenManga = &list[index];
    }

    return chosenManga;
}

auto get_manga_from_mangaupdates(std::string_view name) -> Manga
{
    auto link = mangaupdates_find_manga_url(name);

    auto manga = Manga {};
    manga.name = link.name;
    manga.id = get_id_from_url(link.url);

    return manga;
}

auto get_id_from_url(std::string_view url) -> size_t
{
    const auto pos = url.rfind('=');
    if (pos == std::string_view::npos) {
        return 0;
    }

    auto mangaId = size_t {};
    auto [_, errorCode] = std::from_chars(url.data(), url.data() + pos, mangaId);

    // FIXME: Actually handle these.
    assert(errorCode != std::errc::invalid_argument);
    assert(errorCode != std::errc::result_out_of_range);

    return mangaId;
}

auto mangaupdates_find_manga_url(std::string_view name) -> Link
{
    using namespace std::string_literals;
    auto postFields = "act=series&stype=title&search="s.append(name.data(), name.size());

    if (!curl)
        login();
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postFields.c_str());

    auto document = HtmlDocument { "https://www.mangaupdates.com/series.html" };

    auto const listNode = find_list_node(document.document(), HtmlListPage::SEARCH);
    assert(listNode);

    auto linkList = LinkList {};

    for (auto mangaNode = tidyGetChild(listNode);
         mangaNode;
         mangaNode = tidyGetNext(mangaNode)) {
        for (auto columnNode = tidyGetChild(tidyGetChild(mangaNode));
             columnNode;
             columnNode = tidyGetNext(columnNode)) {
            auto const tagId = tidyNodeGetId(columnNode);
            if (tagId != TidyTag_A) {
                continue;
            }

            // find node with href and name
            auto const altAttr = tidyAttrGetById(columnNode, TidyAttr_ALT);
            if (!altAttr) {
                /* std::cerr << "Error: mangaId attribute could not be found\n"; */
                continue;
            }

            auto const hrefAttr = tidyAttrGetById(columnNode, TidyAttr_HREF);
            if (!hrefAttr) {
                continue;
            }

            auto hrefStr = tidyAttrValue(hrefAttr);
            if (!(*hrefStr)) {
                std::cerr << "Error: mangaId attribute value is empty string\n";
                continue;
            }

            auto url = std::string(hrefStr);

            auto nameNode = columnNode;

            // sometimes the name is in an <i> block, so loop until its just text
            do {
                nameNode = tidyGetChild(nameNode);
            } while (tidyNodeGetId(nameNode));

            auto nameBuf = ManagedTidyBuffer {};
            tidyNodeGetValue(document.document(), nameNode, &nameBuf.get());
            if (!nameBuf.get().size) {
                std::cerr << "Error: Manga title string is null\n";
                break;
            }
            auto nameBufCpy = std::string(reinterpret_cast<char*>(nameBuf.get().bp));

            linkList.push_back({ std::move(url), std::move(nameBufCpy) });
        }
    }

    std::cout << "\n 0: quit\n\n";
    auto i = 0;
    for (auto const& link : linkList) {
        std::cout << std::setw(2) << ++i << ": " << link.name << '\n';
    }

    std::cout << std::flush;

    size_t choice = 0;

    while (!scanf("%zu", &choice)) {
        int ch;
        do {
            ch = getchar();
        } while (ch != '\n' && ch != EOF);
    }

    auto link = Link {};
    if (choice > 0 && choice <= linkList.size()) {
        link = std::move(linkList[choice - 1]);
    }

    return link;
}

auto add_manga_to_list(Manga const& manga, MangaList& list) -> void
{
    list.push_back(manga);
}

// make library
//
// necessary
//
// init_list();
// load_from_json(filename);
// pull();
// add(partofname);
// id = get_id_from_name(partofname);
// update_manga(id, newreadchapter);
// update_manga_fully(id);
// save_to_json(filename);
// push();
//
// unnecessary
//
// display_all_manga();
// display_fully_read_manga();
// display_available_manga();

// checklist
//
// -displaying status of specific manga (found by regex)?
// -only show manga with available chapters when no args, show all with special arg?
// -add ability to add new manga to list

// RETURNS THE NODE FOR THE MANGA LIST

auto find_list_node(TidyDoc tree, HtmlListPage const page) -> TidyNode
{
    auto rootNode = tidyGetBody(tree);
    auto currNode = rootNode;
    while (true) {
        // drill down tree until no children can be found and delete the node
        auto childNode = tidyGetChild(currNode);
        if (!childNode) {
            // if the tree is empty from the root down, return 0
            if (currNode == rootNode) {
                return 0;
            }

            childNode = currNode;
            currNode = tidyGetParent(currNode);
            tidyDiscardElement(tree, childNode);
            continue;
        }

        currNode = childNode;

        if (tidyNodeGetId(currNode) == TidyTag_TABLE) {
            auto targetAttrID = TidyAttrId {};
            auto targetAttrValue = ""sv;

            switch (page) {
            case HtmlListPage::USERLIST:
                targetAttrID = TidyAttr_ID;
                targetAttrValue = "list_table";
                break;
            case HtmlListPage::SEARCH:
                targetAttrID = TidyAttr_CLASS;
                targetAttrValue = "text series_rows_table";
                break;
            default:
                std::cerr << "Err: invalid page argument supplied\n";
                return 0;
                break;
            }

            auto attribute = tidyAttrGetById(currNode, targetAttrID);
            if (attribute) {
                if (tidyAttrValue(attribute) == targetAttrValue) {
                    return currNode;
                }
            }
        }
    }
}

// Write all the html text received from curl to a tidy-buffer

auto tidy_buffer_write_callback(void* in, size_t const size, size_t const nmemb, void* out) -> size_t
{
    unsigned int const realsize = size * nmemb;
    auto buf = static_cast<TidyBuffer*>(out);
    tidyBufAppend(buf, in, realsize);
    return realsize;
}

// For when the output should be ignored

auto noop_tidy_buffer_write_callback(void const*, size_t size, size_t nmemb, void const*) -> size_t
{
    return size * nmemb;
}

auto get_manga_title_from_node(TidyDoc tdoc, TidyNode node) -> std::optional<std::string>
{
    auto titleBuf = ManagedTidyBuffer {};
    tidyNodeGetValue(tdoc, node, &titleBuf.get());
    if (!titleBuf.get().size) {
        std::cerr << "Error: Manga title string is null\n";
        return {};
    }
    return { reinterpret_cast<char*>(titleBuf.get().bp) };
}

auto find_manga_title_node(TidyNode const rootNode) -> TidyNode
{
    for (auto node = tidyGetChild(rootNode);
         node;
         node = tidyGetNext(node)) {
        auto const tagId = tidyNodeGetId(node);
        if (tagId != TidyTag_A) {
            continue;
        }

        auto const titleNode = tidyGetChild(tidyGetChild(node));
        if (!titleNode) {
            std::cerr << "Error: manga title node could not be found\n";
            break;
        }
        return titleNode;
    }
    return nullptr;
}

auto get_latest_chapter_from_node(TidyDoc tdoc, TidyNode node, Manga* manga) -> bool
{
    assert(manga);

    auto chBuf = ManagedTidyBuffer {};
    tidyNodeGetValue(tdoc, node, &chBuf.get());
    if (!chBuf.get().size) {
        std::cerr << "LOG - " << manga->name << ": Latest chapter string is null\n";
        manga->totalCh = 0;
        return true;
    } else if (chBuf.get().size < 4) {
        std::cerr << "Error: Latest manga chapter string is too small (not \"(c.xx)\" format)\n";
        return false;
    }

    // The string containing the latest chapter value is in
    // the format "(c.xx)". the ')' is made into a null character
    // and the parsing starts from the fourth character.
    *(chBuf.get().bp + chBuf.get().size - 1) = 0;
    manga->totalCh = atoi((char*)chBuf.get().bp + 3);
    if (!manga->totalCh) {
        std::cerr << "Error: chapters is 0\n";
        return false;
    }

    return true;
}

auto find_latest_chapter_node(TidyNode rootNode) -> TidyNode
{
    for (auto node = tidyGetChild(rootNode);
         node;
         node = tidyGetNext(node)) {
        // check if the node is right
        auto const tagId = tidyNodeGetId(node);
        if (tagId != TidyTag_SPAN) {
            continue;
        }

        auto const chNode = tidyGetChild(tidyGetChild(node));
        if (!chNode) {
            std::cerr << "Error: Latest chapter node could not be found\n";
            break;
        }
        return chNode;
    }
    return nullptr;
}

// updates the .title, .totalCh, and .readCh fields in the manga struct
// and returns true if successful

auto get_manga_from_node(TidyDoc const tdoc, TidyNode const mangaNode) -> std::optional<Manga>
{
    auto manga = Manga {};

    if (auto id = get_manga_id_from_node(mangaNode); id) {
        manga.id = *id;
    } else {
        return {};
    }

    for (auto rootNode = tidyGetChild(mangaNode);
         rootNode;
         rootNode = tidyGetNext(rootNode)) {
        auto const attr = tidyAttrGetById(rootNode, TidyAttr_CLASS);
        if (!attr || tidyAttrValue(attr) != "lcol_nopri text"sv) {
            continue;
        }

        auto titleNode = find_manga_title_node(rootNode);
        if (!titleNode) {
            std::cerr << "Could not find manga title\n";
            break;
        }
        if (auto title = get_manga_title_from_node(tdoc, titleNode); title) {
            manga.name = std::move(*title);
        } else {
            break;
        }

        auto latestChNode = find_latest_chapter_node(rootNode);
        if (!latestChNode) {
            // this is common since latest chapter only shows up if it is unread
            /* std::cerr << "Could not find latest chapter\n"; */
        }
        if (!get_latest_chapter_from_node(tdoc, latestChNode, &manga)) {
            break;
        }

        auto const readChRootNode = find_read_chapters_node(tidyGetNext(rootNode));
        if (!readChRootNode) {
            std::cerr << "Could not find read chapters\n";
            break;
        }
        if (auto readChapters = get_read_chapters_from_node(tdoc, readChRootNode); readChapters) {
            manga.readCh = *readChapters;
        } else {
            break;
        }

        // @HACK: if latest chapter was not found, set it to read chapters
        if (!manga.totalCh) {
            manga.totalCh = manga.readCh;
        }

        return { std::move(manga) };
    }

    return {};
}

auto get_manga_id_from_node(TidyNode const mangaNode) -> std::optional<size_t>
{
    auto const idAttr = tidyAttrGetById(mangaNode, TidyAttr_ID);
    if (!idAttr) {
        /* std::cerr << "Error: mangaId attribute could not be found\n"; */
        return {};
    }

    auto idStr = tidyAttrValue(idAttr);
    if (!idStr || !(*idStr)) {
        std::cerr << "Error: mangaId attribute value is empty string\n";
        return {};
    }

    auto tmp = atoi(idStr + 1);
    if (!tmp) {
        std::cerr << "Error: id is 0\n";
        return {};
    }

    return { tmp };
}

auto get_read_chapters_from_node(TidyDoc const tdoc, TidyNode const node) -> std::optional<size_t>
{
    auto readChBuf = ManagedTidyBuffer {};

    tidyNodeGetValue(tdoc, node, &readChBuf.get());

    if (!readChBuf.get().size) {
        std::cerr << "Error: read chapters string is null\n";
        return false;
    } else if (readChBuf.get().size < 3) {
        std::cerr << "Error: read chapters string is too small (not \"c.xx\" format\n";
        return false;
    }

    // The string containing the read chapters value is in
    // the format "c.xx". The parsing starts from the third
    // character.
    //
    // To make sure the "latest chapter" element is always present on
    // the website, the website read chapter count is always one less
    // than the actual value read.
    auto readChapters = atoi((char*)readChBuf.get().bp + 2) + 1;
    if (!readChapters) {
        std::cerr << "Error: readCh atoi returned 0\n";
        return {};
    }
    return { readChapters };
}

auto find_read_chapters_node(TidyNode rootNode) -> TidyNode
{
    for (auto node = tidyGetChild(rootNode);
         node;
         node = tidyGetNext(node)) {
        auto const tagId = tidyNodeGetId(node);
        if (tagId != TidyTag_A) {
            continue;
        }

        auto const titleAttr = tidyAttrGetById(node, TidyAttr_TITLE);
        if (!titleAttr || tidyAttrValue(titleAttr) != "Increment Chapter"sv) {
            continue;
        }

        auto readChNode = tidyGetChild(tidyGetChild(tidyGetChild(node)));

        if (!readChNode) {
            std::cerr << "Error: readCh node could not be found\n";
            break;
        }
        return readChNode;
    }
    return nullptr;
}

auto create_credentials_file(std::string_view filename) -> void
{
    assert(filename.data());

    auto credentials = fopen(filename.data(), "w");
    if (!credentials) {
        std::cerr << "Error: can't open " << filename << " for writing\n";
        return;
    }

    fmt::print(credentials, "username=\npassword=");

    fclose(credentials);
}

auto read_credentials(FILE* file) -> void
{
    assert(file);
    assert(curl);

    char username[128] = {};
    char password[128] = {};
    fscanf(file, "username=%[^&\n]", username);
    int ch;
    while ((ch = fgetc(file)) != '\n' && ch != EOF) {
    }

    fscanf(file, "password=%[^&]", password);

    auto postFields = "act=login&username="s + username + "&password=" + password;

    curl_easy_setopt(curl, CURLOPT_COPYPOSTFIELDS, postFields.c_str());
}

auto visit_site() -> void
{
    std::cerr << "Connecting . . .\n";
#ifdef DEBUG
    /* curl_easy_setopt(curl, CURLOPT_VERBOSE, 1); */
#endif
    curl_easy_perform(curl);
}

// login using cookies if available, otherwise
// authenticate using local credentials.txt file

auto login(void) -> void
{
    curl = curl_easy_init();
    assert(curl);

    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0); // if not 0 doesnt work NOTE: not fully secure
    curl_easy_setopt(curl, CURLOPT_COOKIEJAR, cookieFilename);

    auto cookies = fopen(cookieFilename, "r");
    if (cookies) {
        curl_easy_setopt(curl, CURLOPT_COOKIEFILE, cookieFilename);
        fclose(cookies);
    } else {
        curl_easy_setopt(curl, CURLOPT_URL, "https://www.mangaupdates.com/login.html");
        auto credentialsFile = fopen(credentialsFilename, "r");
        if (!credentialsFile) {
            std::cerr << "Error: need a credentials file to login\n";
            create_credentials_file(credentialsFilename);
            reset_curl();
            return;
        }

        read_credentials(credentialsFile);
        visit_site();
    }

    reset_curl();
}

auto save_list(MangaList const& list) -> bool
{
    return json_save_list(list, mangaListFilename);
}

auto find_regex_manga(std::string_view searchName, MangaList const& list) -> std::vector<size_t>
{
    auto stack = std::vector<size_t> {};
    for (auto i = 0ull; i < list.size(); ++i) {
        auto const& manga = list[i];

        if (manga.name.find(searchName) != std::string::npos) {
            stack.push_back(i);
        }
    }
    return stack;
}

auto reset_curl(void) -> void
{
    assert(curl);

    curl_easy_reset(curl);
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0); // if not 0 doesnt work NOTE: not fully secure
}

} // namespace mud
