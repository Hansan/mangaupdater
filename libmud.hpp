#pragma once

#include <string>
#include <vector>
#include <optional>

struct Manga {
    std::string name;
    size_t id;
    size_t totalCh;
    size_t readCh;
};

using MangaList = std::vector<Manga>;

namespace mud {

auto load_list() -> std::optional<MangaList>;
auto save_list(MangaList const& list) -> bool;
auto download_list() -> MangaList;
auto update_list(MangaList& currentList, MangaList& updatedList) -> bool;
auto upload_list(MangaList& mangaList) -> void;
auto get_manga_from_list(std::string_view name, MangaList& mangaList) -> Manga*;
auto get_manga_from_mangaupdates(std::string_view name) -> Manga;
auto add_manga_to_list(Manga const& manga, MangaList& list) -> void;

} // namespace mud
