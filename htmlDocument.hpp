#pragma once

#include "managedTidy.hpp"

#include <tidy.h>

#include <string>

namespace mud {

class HtmlDocument {
public:
    explicit HtmlDocument(std::string url);
    HtmlDocument(HtmlDocument const&) = delete;
    HtmlDocument(HtmlDocument&&) = delete;

    auto operator=(HtmlDocument const&) = delete;
    auto operator=(HtmlDocument&&) = delete;

    auto document() const noexcept -> TidyDoc;

    auto setErrorBuffer(ManagedTidyBuffer& errBuf)
    {
        tidySetErrorBuffer(m_document, &errBuf.get());
    }

    ~HtmlDocument() noexcept;

private:
    TidyDoc m_document = nullptr;
    std::string m_url;
};

}
