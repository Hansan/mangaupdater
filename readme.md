This program/library allows for the manipulation of a manga reading list. It
uses [MangaUpdates](https://www.mangaupdates.com) to get information about
chapters available and is able to sync with the default list provided by the
website.

Dependencies
------------

* libcurl
* libtidy
