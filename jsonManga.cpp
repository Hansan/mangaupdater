#include "jsonManga.hpp"

#include "libmud.hpp"

#include "json.h"

#include <fmt/core.h>

#include <cassert>
#include <cstdio>
#include <filesystem>
#include <iostream>
#include <optional>
#include <string_view>

using namespace std::string_view_literals;

auto json_make_string(std::string_view str) -> json_string_s*;
auto json_make_manga(Manga const& manga) -> json_value_s*;
auto json_make_object_element(std::string_view namestr, std::string_view valuestr, json_type_e type) -> json_object_element_s*;

// save pointers to all nodes used to build the json string so they can be freed
auto jsonstructptrs = std::vector<void*> {};

auto json_load_list(std::filesystem::path file) -> std::optional<MangaList>
{
    MangaList list;

    auto mangaListFile = fopen(file.string().c_str(), "rb");
    if (!mangaListFile) {
        std::cerr << "error opening mangaList.json for reading\n";
        return {};
    }

    fseek(mangaListFile, 0, SEEK_END);
    auto tmp = ftell(mangaListFile);
    if (tmp < 0) {
        std::cerr << "ftell returned " << tmp << '\n';
        return {};
    }
    size_t size = tmp;
    rewind(mangaListFile);

    auto str = std::make_unique<char[]>(size + 1);
    auto chRead = fread(str.get(), 1, size, mangaListFile);
    str.get()[size] = '\0';
    fclose(mangaListFile);
    if (chRead != size) {
        std::cerr << "err loading from json\n";
        return {};
    }

    auto root = json_parse(str.get(), size);
    if (!root) {
        std::cerr << "ERR: could not parse json file\n";
        return {};
    } else {
        std::cerr << "loading from " << file << '\n';
    }
    assert(root->type == json_type_array);
    auto arr = static_cast<struct json_array_s*>(root->payload);
    list.reserve(arr->length);

    for (auto elem = arr->start;
         elem;
         elem = elem->next) {
        assert(elem->value->type == json_type_object);
        auto obj = static_cast<json_object_s*>(elem->value->payload);

        auto manga = Manga {};
        for (auto objelem = obj->start;
             objelem;
             objelem = objelem->next) {
            auto name = objelem->name;
            if (name->string == "name"sv) {
                assert(objelem->value->type == json_type_string);
                auto mangaName = static_cast<json_string_s*>(objelem->value->payload);
                manga.name = mangaName->string;
            } else if (name->string == "id"sv) {
                assert(objelem->value->type == json_type_number);
                auto number = static_cast<json_number_s*>(objelem->value->payload);
                manga.id = atoi(number->number);
            } else if (name->string == "totalCh"sv) {
                assert(objelem->value->type == json_type_number);
                auto number = static_cast<json_number_s*>(objelem->value->payload);
                manga.totalCh = atoi(number->number);
            } else if (name->string == "readCh"sv) {
                assert(objelem->value->type == json_type_number);
                auto number = static_cast<json_number_s*>(objelem->value->payload);
                manga.readCh = atoi(number->number);
            } else {
                std::cerr << "ERR: unknown objelem name\n";
            }
        }
        list.push_back(manga);
    }

    return { std::move(list) };
}

auto json_save_list(MangaList const& list, std::filesystem::path file) -> bool
{
    auto mangaListFile = fopen(file.string().c_str(), "wb");
    if (!mangaListFile) {
        std::cerr << "error writing to " << file << '\n';
        return false;
    }

    json_array_element_s* prevarrelem = nullptr;

    for (auto& manga : list) {
        auto arrelem = new json_array_element_s;
        *arrelem = { json_make_manga(manga), prevarrelem };
        jsonstructptrs.push_back(arrelem);

        prevarrelem = arrelem;
    }

    auto arr = json_array_s { prevarrelem, list.size() };
    auto value = json_value_s { &arr, json_type_array };

    auto size = size_t { 0 };
    auto string = json_write_pretty(&value, nullptr, nullptr, &size);

    std::cerr << "Writing " << file << '\n';
    fwrite(string, 1, size - 1, mangaListFile);

    free(string);
    for (auto ptr : jsonstructptrs) {
        free(ptr);
    }

    return true;
}

auto json_make_string(std::string_view str) -> json_string_s*
{
    auto buflen = str.length() + 1;
    auto buf = new char[buflen] {};
    fmt::format_to_n(buf, buflen - 1, "{}", str);
    auto jsonstring = new json_string_s;
    *jsonstring = { buf, str.length() };
    jsonstructptrs.push_back(jsonstring);
    jsonstructptrs.push_back(buf);

    return jsonstring;
}

auto json_make_object_element(std::string_view namestr,
                              std::string_view valuestr,
                              json_type_e type) -> json_object_element_s*
{
    auto name = json_make_string(namestr);
    auto string = json_make_string(valuestr);
    auto value = new json_value_s;
    *value = { string, static_cast<size_t>(type) };
    auto elem = new json_object_element_s;
    *elem = { name, value, nullptr };
    jsonstructptrs.push_back(value);
    jsonstructptrs.push_back(elem);

    return elem;
}

auto json_make_manga(Manga const& manga) -> json_value_s*
{
    auto nameelem = json_make_object_element("name", manga.name, json_type_string);
    char buf[24] {};

    fmt::format_to_n(buf, sizeof(buf) - 1, "{}", manga.id);
    auto idelem = json_make_object_element("id", buf, json_type_number);
    idelem->next = nameelem;

    fmt::format_to_n(buf, sizeof(buf) - 1, "{}", manga.totalCh);
    auto chelem = json_make_object_element("totalCh", buf, json_type_number);
    chelem->next = idelem;

    fmt::format_to_n(buf, sizeof(buf) - 1, "{}", manga.readCh);
    auto readchelem = json_make_object_element("readCh", buf, json_type_number);
    readchelem->next = chelem;

    auto mangaobj = new json_object_s;
    *mangaobj = { readchelem, 4 };

    auto mangavalue = new json_value_s;
    *mangavalue = { mangaobj, json_type_object };

    jsonstructptrs.push_back(mangaobj);
    jsonstructptrs.push_back(mangavalue);

    return mangavalue;
}
