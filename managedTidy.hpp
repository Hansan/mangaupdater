#pragma once

#include <tidy.h>
#include <tidybuffio.h>

namespace mud {

class ManagedTidyBuffer {
public:
    ManagedTidyBuffer()
    {
        tidyBufInit(&m_buffer);
    }

    ManagedTidyBuffer(ManagedTidyBuffer const&) = delete;
    ManagedTidyBuffer(ManagedTidyBuffer&&) = delete;

    auto operator=(ManagedTidyBuffer const&) = delete;
    auto operator=(ManagedTidyBuffer&&) = delete;

    auto get() noexcept -> TidyBuffer&
    {
        return m_buffer;
    }

    ~ManagedTidyBuffer() noexcept
    {
        tidyBufFree(&m_buffer);
    }

private:
    TidyBuffer m_buffer {};
};

}
