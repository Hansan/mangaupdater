#include "htmlDocument.hpp"

#include <tidy.h>

namespace mud {

auto HtmlDocument::document() const noexcept -> TidyDoc
{
    return m_document;
}

HtmlDocument::~HtmlDocument() noexcept
{
    tidyRelease(m_document);
}

}
